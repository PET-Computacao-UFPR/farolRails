class DocumentController < ApplicationController

	load_and_authorize_resource

    before_action :authenticate_user!

	def index

	end

	def create
		params.permit!
		# @document = Document.new(params.require(:document).permit(:teacher, :subject, :year, :typedoc, :tags0, :tags1))
	 #    @document.file = @document.typedoc+"_"+@document.subject+"_"+@document.teacher+"_"+@document.year.to_s+"_"+Time.now.to_s + Time.now.usec.to_s+".pdf"
		# File.open("db/documents/"+@document.file, 'wb') { |f| f.write( params[:document][:datafile].read ) }
		
		# @document.user = current_user
		# @document.approved = false

		# @tag0nome = params[:document][:tags0]
		# @tag1nome = params[:document][:tags1]
		
		# if not @document.save
		# 	render "create"
		# end

		# if @tag0nome != ""
		# 	# Checar tag antes de aprova-lá
		# 	@document.tags.create(nome: @tag0nome, status: true ) 
		# end

		# if @tag1nome != ""
		# 	# Checar tag antes de aprova-lá
		# 	@document.tags.create(nome: @tag1nome, status: true ) 
		# end	

		redirect_to route_for(:home)
	end
  
	def delete
		@document = Document.find(params[:id])
		File.delete("db/documents/"+@document.file);
		@document.destroy
	end
	
	def approve
		@document = Document.find(params[:id])
		@document.approved = true
		@document.save
	end
	
	def edit
		@document = Document.find(params[:id])
	end
	
	def update
		
		@document = Document.find(params[:id])
		if not @document.update(params[:document].permit(:teacher, :subject, :year, :typedoc))
			render "edit"
		end

		redirect_to "/moderate"
	end
end
