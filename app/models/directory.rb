class Directory < ApplicationRecord
  has_many :childs, class_name: "Directory", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Directory"
  has_many :documents
end
