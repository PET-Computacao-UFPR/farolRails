class Tag < ApplicationRecord
  belongs_to :tag_category
  has_and_belongs_to_many :documents
end
