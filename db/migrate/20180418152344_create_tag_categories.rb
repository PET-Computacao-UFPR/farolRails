class CreateTagCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :tag_categories, force: :cascade do |t|
      t.string :name
      t.boolean :show_on_search
      t.timestamps
    end
  end
end
