class CreateTags < ActiveRecord::Migration[5.1]
  def change
    create_table :tags, force: :cascade do |t|
      t.string :name
      t.references :tag_categories, foreign_key: true
      t.timestamps
    end
  end
end
