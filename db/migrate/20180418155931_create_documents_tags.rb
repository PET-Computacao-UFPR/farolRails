class CreateDocumentsTags < ActiveRecord::Migration[5.1]
  def change
    create_table :documents_tags, id: false, force: :cascade do |t|
      t.integer :document_id, null: false
      t.integer :tag_id, null: false
    end
  end
end
